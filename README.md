# Zombie on a Bicycle

My first time using Unity's Animation Rigging package, so I wanted it to be fun.
This project is not VR-specific.  Open the ZombieBicycle scene and press play.


## Authors and acknowledgment
Shoutout to CodeMonkey for his excellent video that got me started.

## License
Free.  Have fun with it.

## Project status
This is complete for now.  I may add hip flex later.
